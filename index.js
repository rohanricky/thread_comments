
const express = require('express');
const { nanoid } = require('nanoid');
// var User = require('./models/user');
// var session = require('express-session');
var cookieParser = require('cookie-parser')
const MongoConnector = require('./db/MongoConnector');
const app = express();
const fs = require('fs');

app.use(express.urlencoded({extended: true}));
app.use(express.json())

app.use(cookieParser())

global.postId = "1:";

let authObj = {};
let val = fs.readFile("./cookie_user_map.json", (err, jsonString) => {
  if(err) {
    console.log("Cookie file read error!");
    process.exit(1);
  }

  authObj = JSON.parse(jsonString);
});


function loggedIn(req, res, next) {
  if (req.cookies.userId && Object.keys(authObj).includes(req.cookies.userId)) {
    next();
  } else {
    res.status(200).json('Not logged in!');
  }
  // if (req.session.userId) {
  //     next();
  // } else {
  //     res.redirect('/register');
  // }
  // next();
}

app.get('/comment',loggedIn,async (req,res,next) => {
  console.log(req.query.commentId);

  let commentId = req.query.commentId;
  let sort_order = req.query.sort;

  let allComments = await MongoConnector.getComments(commentId ? commentId : "");
  res.status(200).json(allComments);
});

app.post('/comment',loggedIn,async (req,res,next) => {

  let action = req.body.action;
  let commentId = req.body.commentId;

  if (action && action!="") {

    if(action=="add") {
      let commentId="";
      if(req.body.parentCommentId) commentId+=global.postId+req.body.parentCommentId+nanoid(4);
      else commentId=global.postId+nanoid(4);

      let ownerId = req.cookies.userId;
      await MongoConnector.addComment(req.body.comment, commentId, ownerId);
    }

    if(action=="delete") {
      await MongoConnector.modifyComment("--deleted--",req.body.commentId,true);
    }

    if(action=="edit") {
      //edit only if no reply exists
      let allComments = await MongoConnector.getComments(commentId,false);

      if(!MongoConnector.isCommentOwner(allComments,commentId, req.cookies.userId)) {
        return res.status(200).json("Not comment owner");
      }

      if (allComments.length>1) return res.status(200).json("Cannot edit, comment has replies");
      await MongoConnector.modifyComment(req.body.comment, commentId);
    }
  }

  return res.status(200).end();
})

// app.route('/register')
//     .get((req,res,next)=>{
//         res.sendFile('html/register.html',{ root: __dirname });
//       })
//     .post((req,res,next)=>{
//       if(req.body.name && req.body.username && req.body.password){
//         var userData = {
//           username: req.body.username,
//           password: req.body.password,
//         }

//         User.create(userData, function (err, user) {
//           if (err) {
//             return next(err)
//           } else {
//             req.session.userId = user._id;
//             return res.redirect('/profile'); // redirect to login / profile directly
//           }
//         });
//       }
//       });

app.listen(1234,() => console.log("Server running"));