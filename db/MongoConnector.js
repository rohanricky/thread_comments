const MongoClient = require('mongodb').MongoClient;

const CONNECTION_STRING = process.argv[2];
const client = new MongoClient(CONNECTION_STRING, { useNewUrlParser: true, useUnifiedTopology: true });

let allDocs = [];

let appendEachDocument = (document) => {
  allDocs.push(document);
};

module.exports.addComment = (comment,commentId, ownerId) => {
  return new Promise((resolve, reject) => {
    client.connect(async err => {
      const collection = client.db("thread_comments").collection("comments");
      
      await collection.insertOne({"comment":comment,"commentId":commentId, "owner": ownerId, "date": new Date()});
      resolve();
    });
  });
}

module.exports.modifyComment = (newComment,commentId,deleted) => {
  client.connect(async err => {
    const collection = client.db("thread_comments").collection("comments");

    const updateDoc = {
      $set : {
        comment : newComment,
        date : new Date(),
        deleted : deleted
      }
    };
    
    const result = await collection.updateOne({commentId:global.postId+commentId},updateDoc);
    console.log(`${result.matchedCount}`);
  });
};

module.exports.deleteComment = (commentId) => {
  client.connect(async err => {
    const collection = client.db("thread_comments").collection("comments");
    
    const result=await collection.deleteOne({_id:ObjectId(commentId)});

    console.log(`Document deleted ${result.deletedCount}`);
  });
}

module.exports.isCommentOwner = (allDocs,commentId,ownerId) => {

  for(let doc in allDocs) {
    let commentId1 = allDocs[doc]['commentId'];
    if(allDocs[doc]['commentId']==global.postId+commentId) {
      console.log(commentId1,commentId);
      return allDocs[doc]['owner'] == ownerId;
    }
  }

  return false;
}

module.exports.getComments = (prefix) => {

  return new Promise((resolve, reject) => {
    allDocs = [];
    client.connect(async err => {
      const collection = client.db("thread_comments").collection("comments");

      const cursor= collection.find({"commentId":{$regex:global.postId+prefix+"*"}});

      if ((await cursor.count()) === 0) {
        console.log("No documents found!");
        resolve([]);
      }
      // replace console.dir with your callback to access individual elements
      await cursor.forEach(appendEachDocument);
      resolve(allDocs);
    });
  });
}
